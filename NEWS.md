Release 1.1.0-dev
YYYY-MM-DD

 * Important / Breaking Changes *

 - Support for tolerance/precision=0 added, -1 is the new value
   for automatic computation of minimal tolerance.

 * New Features *

Release 1.0.0
2016-05-19

  Initial release
